# limehome Challenge - Backend Engineer

Here at limehome we use both Typescript and Python, which means you are free to use whichever you are most comfortable in. In order to keep the challenge small and self-contained the technologies and practices in the challenges do not represent the standards that we use for our other projects.

The challenge context has been reproduced below, but it is also available on all of the available repositories.

[Python Challenge](https://gitlab.com/limehome/interviews/backend-challenge-python)

[Typescript Challenge](https://gitlab.com/limehome/interviews/backend-challenge-typescript)

We want to be respectful of your time and recent experiences which is why we also offer the challenge in [Java](https://gitlab.com/limehome/interviews/backend-challenge-java). It should be noted that we still require expertise in either TS or Python for this position, which will be evaluated in any follow-up interviews.


## Context

We would like you to help us with a small service that we have for handling bookings. A booking for us simply tells us which guest will be staying in which unit, as well as when they arrive and the number of nights that guest will be enjoying our amazing suites, comfortable beds, great snac.. apologies - I got distracted. Bookings are at the very core of our business and it's important that we get these right - we want to make sure that guests always get what they paid for, and also trying to ensure that our unit are continually booked and have as few empty nights where no-one stays as possible. A unit is simply a location that can be booked, think like a hotel room or even a house. For the exercise today, we would like you to help us solve an issue we've been having with our example service, as well as implement a new feature to improve the code base. While this is an opportunity for you to showcase your skills, we also want to be respectful of your time and suggest spending no more than 3 hours on this (of course you may also spend longer if you feel that is necessary)

### A - You should help us:
Identify and fix a bug that we've been having with bookings - there seems to be something going wrong with the booking process where a guest will arrive at a unit only to find that it's already booked and someone else is there! We have included a unit test to show this (the test is the one that is failing).

There are many ways to solve this bug - there is no single correct answer that we are looking for.

### B - Implement a new feature:
We want to allow guests to extend their stays if possible. It happens that <strike>sometimes</strike> all the time people love staying at our locations so much that they want to extend their stay and remain there a while longer. We'd like a new API that will let them do that

## Set-up
While we provide a choice of projects to work with (either `TS`, `Python`, or `Java`), we understand if you want to implement this in something you're more comfortable with. You are free to re-implement the parts that we have provided in another language, however this may take some time and we would encourage you not spend more time than you're comfortable with!

When implementing, make sure you follow known best practices around architecture, testability, and documentation.

Please see your chosen language's repository for specific instructions on how to run and test the challenge. 

[Python Challenge](https://gitlab.com/limehome/interviews/backend-challenge-python)

[Typescript Challenge](https://gitlab.com/limehome/interviews/backend-challenge-typescript)

[Java Challenge](https://gitlab.com/limehome/interviews/backend-challenge-java)
